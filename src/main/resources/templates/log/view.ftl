<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>管理员列表</title>
<#include "../comm/upms-base.ftl" />
<#include "../comm/upms-layui.ftl" />
    <link rel="stylesheet" href="../../../static/css/global.css"/>
</head>
<body class="shik_body">
<div class="layui-row">

    <div class="layui-col-xs12">
        <table class="layui-table" lay-skin="line">
            <colgroup>
                <col width="6%">
                <col width="40%">
                <col width="8%">
                <col width="8%">
                <col width="15%">
                <col width="15%">
                <col width="8%">
                <col>
            </colgroup>
            <thead>
            <tr>
                <th>发送手机</th>
                <th>发送内容</th>
                <th>发送状态</th>
                <th>发送条数</th>
                <th>回调信息</th>
                <th>发送时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
                <#list smsLog.smsReportList as report>
                    <tr>
                        <td>${report.phone_number}</td>
                        <td>【${smsLog.smsAuthorize.smsSign.title}】${smsLog.smsContent}</td>
                        <td>${report.success?then('成功', '失败')}</td>
                        <td>${report.sms_size}</td>
                        <td>${report.err_msg}</td>
                        <td>${report.send_time}</td>
                        <td><a>重新发送</a></td>
                    </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
</body>
</html>
