/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.service.impl;

import com.shikSMS.constant.ModelApplyConstants;
import com.shikSMS.constant.SysConstants;
import com.shikSMS.domain.SmsAuthorize;
import com.shikSMS.domain.SmsHost;
import com.shikSMS.domain.SmsModel;
import com.shikSMS.domain.SmsModelApply;
import com.shikSMS.manage.service.SmsAuthorizeService;
import com.shikSMS.manage.service.SmsHostService;
import com.shikSMS.manage.service.SmsModelApplyService;
import com.shikSMS.manage.service.SmsModelService;
import com.shikSMS.repository.SmsModelApplyRepository;
import com.shikSMS.support.util.BeanUtils;
import com.shikSMS.support.util.PageUtils;
import com.shikSMS.support.util.smsSend.SmsTemplateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author gengshikun
 * @date 2017/12/26
 */
@Service
@Transactional
public class SmsModelApplyServiceImpl implements SmsModelApplyService {

    @Autowired
    private SmsModelApplyRepository smsModelApplyRepository;

    @Autowired
    private SmsHostService smsHostService;

    @Autowired
    private SmsModelService smsModelService;

    @Autowired
    private SmsAuthorizeService smsAuthorizeService;

    @Override
    public void apply(SmsModelApply smsModelApply, String publicKey) {
        SmsHost smsHost = this.smsHostService.findByPublicKey(publicKey);

        smsModelApply.setVarifyStatus(ModelApplyConstants.SMS_MODEL_VARIFY_STATUS_WAIT);
        smsModelApply.setHostId(smsHost.getId());
        smsModelApply.setNumber(NumberUtils.INTEGER_ZERO);

        this.upsert(smsModelApply);

    }

    @Override
    public void upsert(SmsModelApply smsModelApply) {


        if(StringUtils.isBlank(smsModelApply.getId())) {
            smsModelApply.setCreateTime(System.currentTimeMillis());
            smsModelApply.setDeleteBoolean(Boolean.FALSE);
        } else {
            SmsModelApply oldSmsModelApply = this.smsModelApplyRepository.findOne(smsModelApply.getId());
            smsModelApply.setUpdateTime(System.currentTimeMillis());
            BeanUtils.copyProperties(smsModelApply, oldSmsModelApply);
            smsModelApply = oldSmsModelApply;
        }

        this.smsModelApplyRepository.saveAndFlush(smsModelApply);
    }

    @Override
    public Page getListForPage(String query, Integer pageNo) {
        Page<SmsModelApply> pager = null;

        if (StringUtils.isNotBlank(query)) {
            pager = this.smsModelApplyRepository.findByTitleLikeAndDeleteBoolean(query, Boolean.FALSE, PageUtils.buildPageRequest(pageNo));
        } else {
            pager = this.smsModelApplyRepository.findByDeleteBoolean(Boolean.FALSE, PageUtils.buildPageRequest(pageNo));
        }

        return pager;
    }

    @Override
    public SmsModelApply findOne(String id) {
        return this.smsModelApplyRepository.findOne(id);
    }

    @Override
    public void pass(String id, String signId, String smsCode) {
        SmsModelApply smsModelApply = this.findOne(id);
        smsModelApply.setVarifyStatus(ModelApplyConstants.SMS_MODEL_VARIFY_STATUS_OK);
        smsModelApply.setVarifyTime(System.currentTimeMillis());
        smsModelApply.setSignId(signId);
        this.upsert(smsModelApply);

        // 创建对应模板
        SmsModel smsModel = new SmsModel(smsModelApply.getTitle(), SysConstants.SMS_MODEL_TYPE_ALI, smsCode, smsModelApply.getModelContent());
        smsModel = this.smsModelService.upsert(smsModel);

        SmsAuthorize smsAuthorize = new SmsAuthorize(smsModelApply.getTitle(), Boolean.TRUE, smsModel.getId(), smsModelApply.getSignId(), smsModelApply.getHostId());
        smsAuthorize = this.smsAuthorizeService.upsert(smsAuthorize);

        // 回调 发送sendKey
        try {
            SmsTemplateUtils.smsPass(smsModelApply.getReturnUrl(), smsModelApply.getTemplateId(), smsAuthorize.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refuse(String id, String refuseReason) {
        SmsModelApply smsModelApply = this.findOne(id);
        smsModelApply.setVarifyStatus(ModelApplyConstants.SMS_MODEL_VARIFY_STATUS_REFUSE);
        smsModelApply.setVarifyTime(System.currentTimeMillis());
        smsModelApply.setRefuseReason(refuseReason);
        this.upsert(smsModelApply);

        // 回调 发送拒绝原因
        try {
            SmsTemplateUtils.smsRefuse(smsModelApply.getReturnUrl(), smsModelApply.getTemplateId(), smsModelApply.getRefuseReason());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
