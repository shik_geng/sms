<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>管理员添加</title>
    <#include "../comm/upms-base.ftl" />
    <#include "../comm/upms-layui.ftl" />
    <link rel="stylesheet" href="../../../static/css/global.css"/>
</head>
<body class="shik_body">
    <div class="layui-row">
        <div class="layui-col-xs12">
            <form id="shik_sign_form" class="layui-form layui-form-pane" action="/sign/upsert" method="post">
                <input type="hidden" name="id" value="${smsSign.id}">
                <div class="layui-form-item">
                    <label class="layui-form-label">短信签名</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" name="title" lay-verify="required"
                               value="${smsSign.title}" autocomplete="off" placeholder="请输入短信签名" />
                    </div>
                </div>
                <div class="layui-form-item" pane>
                    <label class="layui-form-label">是否启用</label>
                    <div class="layui-input-block">
                        <input type="checkbox" ${smsSign.status?then('checked=""', '')} name="status" lay-skin="switch" lay-filter="switchTest" title="开关">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">KeyId</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" name="smsKeyId" lay-verify="required"
                               value="${smsSign.smsKeyId}" autocomplete="off" placeholder="请输入SmsKeyId" />
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">KeySecret</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" name="smsKeySecret" lay-verify="required"
                               value="${smsSign.smsKeySecret}" autocomplete="off" placeholder="请输入SmsKeySecret" />
                    </div>
                </div>
                <div class="layui-layer-btn layui-layer-btn-c">
                    <a class="layui-layer-btn0" lay-submit lay-filter="sign_submit">确认</a>
                    <a class="layui-layer-btn1 sign_cancel" lay-filter="sign_cancel">取消</a>
                </div>
            </form>
        </div>
    </div>
</body>
<script type="text/javascript" src="../../../static/js/sign.js"></script>
<script>
</script>
</html>
