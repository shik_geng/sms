/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.constant;

/**
 * @author gengshikun
 * @date 2016/12/10
 */
public class SysConstants {

    public static final String URL_CHARSET = "UTF-8";

    /********************** permission type start ************************/
    /**
     * 系统
     */
    public static final String PERMISSION_TYPE_A01 = "A01";
    /**
     * 菜单
     */
    public static final String PERMISSION_TYPE_A02 = "A02";
    /**
     * 按钮
     */
    public static final String PERMISSION_TYPE_A03 = "A03";
    /********************** permission type end ************************/

    /********************** sms model type start B ************************/
    public static final String SMS_MODEL_TYPE_ALI = "B00"; //ALI
    public static final String SMS_MODEL_TYPE_253 = "B01"; //253
    /********************** sms model type end B ************************/


    /********************** sort 字段 start ************************/
    public static final String SORT_ASC = "asc"; // 升序
    public static final String SORT_DESC = "desc"; // 降序

    public static final Integer SORT_PAGE_NUMBER = 10; // 通用分页数

    public static final String SORT_CREATE_TIME = "createTime";
    /********************** sort 字段 end ************************/


}
