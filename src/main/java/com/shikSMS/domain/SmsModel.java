/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 短信模板
 * @author gengshikun
 * @date 2016/12/15
 */
@Data
@Entity(name = "sms_model")
public class SmsModel implements Serializable {

    @Id
    @GenericGenerator(name = "idGenerator", strategy = "uuid") //这个是hibernate的注解/生成32位UUID
    @GeneratedValue(generator = "idGenerator")
    @Column(length = 32)
    private String id;

    @Column(nullable = false, length = 32)
    private String title;

    @Column(length = 3)
    private String modelType;

    @Column
    private String smsCode;

    @Column
    private String modelContent;

    @Column
    private Boolean status = Boolean.FALSE;

    @Column(updatable = false)
    private Long createTime;

    @Column
    private Long updateTime;

    @Column
    private Boolean deleteBoolean;

    public SmsModel(){}

    public SmsModel(String title, String modelType, String smsCode, String modelContent) {
        this.title = title;
        this.modelType = modelType;
        this.smsCode = smsCode;
        this.modelContent = modelContent;

        this.status = Boolean.TRUE;
        this.createTime = System.currentTimeMillis();
        this.deleteBoolean = Boolean.FALSE;
    }
}
