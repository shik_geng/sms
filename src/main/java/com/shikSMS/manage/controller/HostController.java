/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.controller;

import com.shikSMS.domain.SmsHost;
import com.shikSMS.manage.service.SmsHostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author gengshikun
 * @date 2017/11/24
 */
@Controller
@RequestMapping(value = "host")
public class HostController {

    private static final Logger logger = LoggerFactory.getLogger(HostController.class);

    @Autowired
    private SmsHostService smsHostService;

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(String query, Integer pageNo, Model model) {
        Page pager = this.smsHostService.getListForPage(query, pageNo);
        model.addAttribute("pager", pager);
        model.addAttribute("pageNo", pageNo == null ? 1 : pageNo);
        return "host/list";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add() {
        return "host/add";
    }

    @RequestMapping(value = "upsert", method = RequestMethod.POST)
    @ResponseBody
    public Boolean save(SmsHost smsHost) {
        this.smsHostService.upsert(smsHost);
        return Boolean.TRUE;
    }

    @RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable String id, Model model) {
        SmsHost smsHost = this.smsHostService.findOne(id);
        model.addAttribute("smsHost", smsHost);
        return "host/edit";
    }



}
