/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.service.impl;

import com.shikSMS.domain.SmsLog;
import com.shikSMS.manage.service.SmsLogService;
import com.shikSMS.repository.SmsLogRepository;
import com.shikSMS.support.util.BeanUtils;
import com.shikSMS.support.util.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author gengshikun
 * @date 2017/11/28
 */
@Service
@Transactional
public class SmsLogServiceImpl implements SmsLogService {

    @Autowired
    private SmsLogRepository smsLogRepository;

    @Override
    public void upsert(SmsLog smsLog) {
        this.smsLogRepository.saveAndFlush(smsLog);
    }

    @Override
    public Page getListForPage(String query, Integer pageNo) {
        Page<SmsLog> pager = null;

        if (StringUtils.isNotBlank(query)) {
            pager = this.smsLogRepository.findByPhone(query, PageUtils.buildPageRequest(pageNo));
        } else {
            pager = this.smsLogRepository.findAll(PageUtils.buildPageRequest(pageNo));
        }

        return pager;
    }

    @Override
    public SmsLog findOneByBizId(String bizId) {
        return this.smsLogRepository.findByBizId(bizId);
    }

    @Override
    public SmsLog findOne(String id) {
        return this.smsLogRepository.findOne(id);
    }
}
