/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.service.impl;

import com.shikSMS.domain.SmsHost;
import com.shikSMS.manage.service.SmsHostService;
import com.shikSMS.repository.SmsHostRepository;
import com.shikSMS.support.plugin.keyCode.RSACoder;
import com.shikSMS.support.util.BeanUtils;
import com.shikSMS.support.util.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author gengshikun
 * @date 2017/11/24
 */
@Service
@Transactional
public class SmsHostServiceImpl implements SmsHostService {

    @Autowired
    private SmsHostRepository smsHostRepository;

    @Override
    public void upsert(SmsHost smsHost) {
        try {
            if(StringUtils.isBlank(smsHost.getId())) {
                smsHost.setNumber(NumberUtils.INTEGER_ZERO);
                Map<String, Object> keyMap = RSACoder.initKey();
                smsHost.setPublicKey(RSACoder.getPublicKey(keyMap));
                smsHost.setPrivateKey(RSACoder.getPrivateKey(keyMap));
                smsHost.setCreateTime(System.currentTimeMillis());
                smsHost.setDeleteBoolean(Boolean.FALSE);
            } else {
                SmsHost oldSmsHost = this.smsHostRepository.findOne(smsHost.getId());
                smsHost.setUpdateTime(System.currentTimeMillis());
                BeanUtils.copyProperties(smsHost, oldSmsHost);
                smsHost = oldSmsHost;
            }
            this.smsHostRepository.saveAndFlush(smsHost);

        } catch (Exception e) {
        }


    }

    @Override
    public Page getListForPage(String query, Integer pageNo) {
        Page<SmsHost> pager = null;

        if (StringUtils.isNotBlank(query)) {
            pager = this.smsHostRepository.findByTitleLikeAndDeleteBoolean(query, Boolean.FALSE, PageUtils.buildPageRequest(pageNo));
        } else {
            pager = this.smsHostRepository.findByDeleteBoolean(Boolean.FALSE, PageUtils.buildPageRequest(pageNo));
        }

        return pager;
    }

    @Override
    public List<SmsHost> findAll() {
        return this.smsHostRepository.findByStatusAndDeleteBoolean(Boolean.TRUE, Boolean.FALSE);
    }

    @Override
    public SmsHost findOne(String id) {
        return this.smsHostRepository.findOne(id);
    }

    @Override
    public SmsHost findByPublicKey(String publicKey) {
        return this.smsHostRepository.findByPublicKey(publicKey);
    }
}
