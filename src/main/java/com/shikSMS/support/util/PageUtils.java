/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.support.util;

import com.shikSMS.constant.SysConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * @author gengshikun
 * @date 2016/12/16
 */
public class PageUtils {

    /**
     * 排序
     *
     * @param pageNo    页码
     * @param pageSize
     * @param sortType  升序 asc 降序 desc 默认desc
     * @param sortParam 排序字段  默认id
     * @return
     */
    public static final PageRequest buildPageRequest(Integer pageNo, Integer pageSize, String sortParam, String sortType) {

        if (pageNo == null || pageNo == 0) {
            pageNo = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = SysConstants.SORT_PAGE_NUMBER;
        }
        if (StringUtils.isBlank(sortParam)) {
            sortParam = SysConstants.SORT_CREATE_TIME;
        }
        if (StringUtils.isBlank(sortType)) {
            sortType = SysConstants.SORT_DESC;
        }


        Sort sort = null;
        if (SysConstants.SORT_DESC.equals(sortType)) {
            sort = new Sort(Sort.Direction.DESC, sortParam);
        } else {
            sort = new Sort(Sort.Direction.ASC, sortParam);
        }

        return new PageRequest(pageNo - 1, pageSize, sort);
    }

    public static final PageRequest buildPageRequest(Integer pageNo, Integer pageSize, String sortParam) {
        return buildPageRequest(pageNo, pageSize, sortParam, SysConstants.SORT_DESC);
    }

    public static final PageRequest buildPageRequest(Integer pageNo, Integer pageSize) {
        return buildPageRequest(pageNo, pageSize, SysConstants.SORT_CREATE_TIME);
    }

    public static final PageRequest buildPageRequest(Integer pageNo) {
        return buildPageRequest(pageNo, SysConstants.SORT_PAGE_NUMBER);
    }
}
