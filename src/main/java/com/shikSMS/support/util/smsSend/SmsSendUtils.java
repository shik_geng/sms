/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.support.util.smsSend;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.crypto.Cipher;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Map;

/**
 * @author gengshikun
 * @date 2017/11/25
 */
public class SmsSendUtils {

    public static final String KEY_ALGORITHM = "RSA";

    /**
     * 加密<br>
     * 用公钥加密
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    public static String encryptByPublicKey(String data, String key)
            throws Exception {
        // 对公钥解密
        byte[] keyBytes = decryptBASE64(key);

        // 取得公钥
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key publicKey = keyFactory.generatePublic(x509KeySpec);

        // 对数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        return SmsSendUtils.encryptBASE64(cipher.doFinal(data.getBytes()));
    }

    /**
     * 解密<br>
     * 用公钥解密
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    public static String decryptByPublicKey(String data, String key)
            throws Exception {
        // 对密钥解密
        byte[] keyBytes = decryptBASE64(key);

        // 取得公钥
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key publicKey = keyFactory.generatePublic(x509KeySpec);

        // 对数据解密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, publicKey);

        return new String(cipher.doFinal(decryptBASE64(data)));
    }

    /**
     * BASE64解密
     *
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] decryptBASE64(String key) throws Exception {
        return Base64.decodeBase64(key);
    }

    /**
     * BASE64加密
     *
     * @param key
     * @return
     * @throws Exception
     */
    public static String encryptBASE64(byte[] key) throws Exception {
        return Base64.encodeBase64String(key);
    }

    /**
     * 调用短信服务
     * @param sendUrl 短信服务地址
     * @param sendKey sendKey
     * @param publicKey 加密公钥
     * @param phone 接收短信手机号
     * @param params 依次传入短信模板参数值
     * @throws Exception
     */
    public static void send(String sendUrl, String sendKey, String publicKey, String phone, String... params) throws Exception {

        SmsSendUtils.send(sendUrl, sendKey, publicKey, new String[]{phone}, params);

    }

    /**
     * 调用短信服务
     * @param sendUrl 短信服务地址
     * @param sendKey sendKey
     * @param publicKey 加密公钥
     * @param phones 接收短信手机号
     * @param params 依次传入短信模板参数值
     * @throws Exception
     */
    public static void send(String sendUrl, String sendKey, String publicKey, String[] phones, String... params) throws Exception {
        //发短信
        Map httpParams = Maps.newHashMap();
        httpParams.put("sendKey", sendKey);
        httpParams.put("publicKey", publicKey);
        httpParams.put("phones", SmsSendUtils.encryptByPublicKey(Arrays.toString(phones), publicKey));
        httpParams.put("params", SmsSendUtils.encryptByPublicKey(Arrays.toString(params), publicKey));
        HttpUtils.postParameters(sendUrl, httpParams);
    }

    public static void main(String[] args) throws Exception {
        String[] phones = new String[]{"13271173210"};

        SmsSendUtils.send(
                "http://127.0.0.1:3331/send",
                "402881a660207e52016020881e6d0009",
                "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCLiT+JtXXry2RRnT2EskleOQtmm1uEDWbWC1t+et3BO7xtNHiB6MDYR6/IIqfGfvOdob63gVAWWasJhtgLaFj+mFkmQTZLEagMS1ApZFJCtZcdRFoQBUbGbK3EtQLU7bgJ4rJH6lyY+eKiu5ZDxTEupJ9p+eG0C32WsNONiOhM4QIDAQAB",
                phones,
                null);
        /*SmsSendUtils.send(
                "http://sms.shiky.net/send",
                "2c9f0a816006eb5c016006ed24320003",
                "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDOczZMZnI249tp3T1bPgM4O1tka4hf64t6cuiQ9rSOR6VkMTIIm1wwRAmXQkkooLAcHKSoSaqNjswM9YSA1qwR10lJQlvE+WjUSzDbMNmMY8YLVJQy1pabpM/DA34hdExUQEHrZzE3qPDPYMfbRvUaoz/qwBiKDrp7tlzj6Ki+2QIDAQAB",
                phones,
                "shiksms-shiky");*/
        /*SmsSendUtils.send(
//                "http://127.0.0.1:8093/send",
                "http://sms.vjuzhen.com/send",
                "8a9e7b8b6044f35b016071978e8b0135",
                "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAgibnf/kA/tike5K2VrFC0QIm0+qXrEU8UVUSuHIPL0MtdSDpvyW1KluyNF4S6/Klswc6yfC3mkqpVcIc7ZbzjTGN05ISiTYyqL0nXTcINBmp7TP93LbKRaWnue45/faegFepfoakYwWLCgaW9eBlgSzeLrLDZbL2EuufTuvCxORQAmj10k/R9oB94p46vxkZ7apjXtL0/N/qS48WLz9mOjYeusvWPG4DbX7Z1adhU8m5YR4T7tsKGkD8mVSnFefmcibsyEPESWn7cQM+e13FjZuZ5KJehuH0mZyxvJQ1E9XDI2qUiuNNzX5iJ2BkKWO8jVO4Ss1Ff3x5UkaBl+GnGN/if3pVuv60bECQOMgaKY79UEUucoxHJpGhz9/OFQ1BRE/A6nUUwfyIz7Fti8ish0emqSHNkUA079oh4DIGVUD1u6XnGXwpSqD4nCglCueFbkNwpJY4nORqWLwafU8PcHcek092Q9No5J7jic+r/2ed+Ld0SttioN8Va3bxQSlJAgMBAAE=",
                phones,
                "减肥的","2017-12-21 15:58:00","这些不为人知却超级好用的神器，推荐给你！","519238747190,162839478871,493102761975,126485304491","yaeIfi");*/
    }

}
