<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>管理员列表</title>
<#include "../comm/upms-base.ftl" />
<#include "../comm/upms-layui.ftl" />
    <link rel="stylesheet" href="../../../static/css/global.css"/>
</head>
<body class="shik_body">
<div class="layui-row">

    <div class="layui-col-xs12">
        <div class="layui-col-md3">
            <a class="layui-btn layui-btn-primary layui-btn-small host_add"><i class="layui-icon">&#xe654;</i></a>
        </div>
    </div>

    <div class="layui-col-xs12">
        <table class="layui-table" lay-skin="line">
            <colgroup>
                <col width="150">
                <col width="150">
                <col width="200">
                <col width="200">
                <col width="200">
                <col>
            </colgroup>
            <thead>
            <tr>
                <th>名称</th>
                <th>已发数量</th>
                <th>剩余数量</th>
                <th>启用状态</th>
                <th>创建时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
                <#list pager.content as host>
                    <tr>
                        <td>${host.title}</td>
                        <td>${host.number}</td>
                        <td>${host.leftNumber}</td>
                        <td>${host.status?then('启用', '禁用')}</td>
                        <td>${host.createTime?number_to_datetime}</td>
                        <td><a class="host_edit" data_id="${host.id}">编辑</a></td>
                    </tr>
                </#list>
            </tbody>
        </table>
    </div>

    <div class="layui-col-xs12">
        <div class="shik_footer" id="shik_page"></div>
    </div>
</div>
</body>
<#include "../comm/sms-page.ftl" />
<script type="text/javascript" src="../../../static/js/host.js"></script>
</html>
