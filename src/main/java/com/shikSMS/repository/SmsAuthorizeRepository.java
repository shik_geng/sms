/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.repository;

import com.shikSMS.domain.SmsAuthorize;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;


/**
 * @author gengshikun
 * @date 2017/11/24
 */
@CacheConfig(cacheNames = "sms:authorize")
public interface SmsAuthorizeRepository extends PagingAndSortingRepository<SmsAuthorize, String>, JpaRepository<SmsAuthorize, String> {

    Page<SmsAuthorize> findByTitleLikeAndDeleteBoolean(String query, Boolean deleteBoolean, Pageable pageable);
    Page<SmsAuthorize> findAllByDeleteBoolean(Boolean deleteBoolean, Pageable pageable);

//    @CacheEvict(key = "#p0.id")
//    SmsAuthorize saveAndFlush(SmsAuthorize id);
//
//    @Cacheable(key = "#p0")
//    SmsAuthorize findOne(String id);
}
