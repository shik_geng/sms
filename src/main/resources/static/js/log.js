/**
 *
 * @author gengshikun
 * @date 2016/12/12
 */
layui.use(['form', 'layer', 'jquery', 'laydate'], function () {
    console.log(parent.layer === undefined)
    var form = layui.form;
        layer = layui.layer;
        $ = layui.jquery;
        laypage = layui.laypage;
        laydate = layui.laydate;

    $('button.shik_btn').on('click', function(){
        var othis = $(this), method = othis.data('method');
        active[method] ? active[method].call(this, 1) : '';
    });

    laydate.render({
        elem: '#date1'
        ,type: 'datetime'
        ,calendar: true
    });

    laydate.render({
        elem: '#date2'
        ,type: 'datetime'
        ,calendar: true
    });

    // log_submit
    form.on('submit(log_submit)', function(data){
        $.post('/log/upsert', data.field, function(){
            closeIframe();
            parent.location.reload(); // 刷新
        }, 'json');
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });

    $('.log_add').on('click', function() {
        layer.open({
            type: 2,
            title: '管理员添加',
            content: ['/log/add', 'no'],
            area: ['440px', '370px']
        });
    });

    $('.log_view').on('click', function() {
        var id = $(this).parents('tr').attr('data_id');
        layer.open({
            type: 2,
            title: false,
            shade: 0.1,
            shadeClose: true,
            closeBtn: 0,
            anim: 2,
            content: '/log/'+id+'/view',
            area: ['100%', '400px'],
            offset: 'b'
        });
    });

    $('.log_cancel').on('click', function() {
        closeIframe();
    })

    function closeIframe() {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
});

//触发事件
var active = {
    query: function(pageNo) {
        console.log('query:'+pageNo);
        console.log(location);

        location.href = location.origin + location.pathname + '?pageNo=' + pageNo + '&' + $('.log_form').serialize();
    }
};
