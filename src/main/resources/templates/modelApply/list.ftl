<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>管理员列表</title>
<#include "../comm/upms-base.ftl" />
<#include "../comm/upms-layui.ftl" />
    <link rel="stylesheet" href="../../../static/css/global.css"/>
</head>
<body class="shik_body">
<div class="layui-row">

    <div class="layui-col-xs12">
        <table class="layui-table" lay-skin="line">
            <colgroup>
                <col width="150">
                <col width="150">
                <col width="200">
                <col>
            </colgroup>
            <thead>
            <tr>
                <th>模板名称</th>
                <th>审核状态</th>
                <th>创建时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
                <#list pager.content as model>
                    <tr>
                        <td>${model.title}</td>
                        <td>
                            <#if model.varifyStatus??>
                                <#switch model.varifyStatus>
                                    <#case '1'>
                                        待审核
                                        <#break>
                                    <#case '2'>
                                        通过
                                        <#break>
                                    <#case '3'>
                                        拒绝
                                        <#break>
                                    <#default>
                                        --
                                </#switch>
                                <#else >
                                --
                            </#if>

                        </td>
                        <td>${model.createTime?number_to_datetime}</td>
                        <td>
                            <#if model.varifyStatus?? && model.varifyStatus == '1'>
                                <a class="model_varify" data_id="${model.id}">审核</a>
                            <#else >
                                <a class="model_info" data_id="${model.id}" data_varify="${model.varifyStatus}">详情</a>
                            </#if>
                        </td>
                    </tr>
                </#list>
            </tbody>
        </table>
    </div>

    <div class="layui-col-xs12">
        <div class="shik_footer" id="shik_page"></div>
    </div>
</div>
</body>
<#include "../comm/sms-page.ftl" />
<script type="text/javascript" src="../../../static/js/model.js"></script>
</html>
