<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>管理员添加</title>
    <#include "../comm/upms-base.ftl" />
    <#include "../comm/upms-layui.ftl" />
    <link rel="stylesheet" href="../../../static/css/global.css"/>
</head>
<body class="shik_body">
    <div class="layui-row">
        <div class="layui-col-xs12">
            <form id="shik_model_form" class="layui-form layui-form-pane" action="/model/upsert" method="post">
                <input type="hidden" name="id" value="${smsModel.id}">
                <div class="layui-form-item">
                    <label class="layui-form-label">模板名称</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" name="title" lay-verify="required"
                               value="${smsModel.title}" autocomplete="off" placeholder="请输入模板名称" />
                    </div>
                </div>
                <div class="layui-form-item" pane>
                    <label class="layui-form-label">是否启用</label>
                    <div class="layui-input-block">
                        <input type="checkbox" ${smsModel.status?then('checked=""', '')} name="status" lay-skin="switch" lay-filter="switchTest" title="开关">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">模板类型</label>
                    <div class="layui-input-block">
                        <select name="modelType" lay-filter="model_type" lay-search="">
                            <option value="B00" ${(smsModel.modelType == 'B00')?then('selected=""', '')}>阿里</option>
                            <option value="B01" ${(smsModel.modelType == 'B01')?then('selected=""', '')}>253</option>
                        </select>
                    </div>
                </div>
                <div>
                    <div class="layui-form-item model_B00">
                        <label class="layui-form-label">模板CODE</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="smsCode" lay-verify="required"
                                   value="${smsModel.smsCode}" autocomplete="off" placeholder="请输入模板CODE" />
                        </div>
                    </div>
                    <div class="layui-form-item layui-form-text model_B01">
                        <label class="layui-form-label">短信模板</label>
                        <div class="layui-input-block">
                            <textarea name="modelContent" placeholder="请输入模板内容" lay-verify="required" class="layui-textarea">${smsModel.modelContent}</textarea>
                        </div>
                    </div>
                </div>
                <div class="layui-layer-btn layui-layer-btn-c">
                    <a class="layui-layer-btn0" lay-submit lay-filter="model_submit">确认</a>
                    <a class="layui-layer-btn1 model_cancel" lay-filter="model_cancel">取消</a>
                </div>
            </form>
        </div>
    </div>
</body>
<script type="text/javascript" src="../../../static/js/model.js"></script>
<script>
</script>
</html>
