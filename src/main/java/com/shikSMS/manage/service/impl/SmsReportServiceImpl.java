/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.shikSMS.domain.SmsHost;
import com.shikSMS.domain.SmsLog;
import com.shikSMS.domain.SmsReport;
import com.shikSMS.manage.service.SmsHostService;
import com.shikSMS.manage.service.SmsLogService;
import com.shikSMS.manage.service.SmsReportService;
import com.shikSMS.repository.SmsReportRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gengshikun
 * @date 2017/12/1
 */
@Service
@Transactional
public class SmsReportServiceImpl implements SmsReportService {

    @Autowired
    private SmsReportRepository smsReportRepository;

    @Autowired
    private SmsLogService smsLogService;

    @Autowired
    private SmsHostService smsHostService;

    @Override
    public void dealReport(String result) throws Exception {
        List<HashMap> list = JSON.parseArray(result, HashMap.class);
        for(Map map : list) {
            System.out.println(map);

            String bizId = (String) map.get("biz_id");
            SmsLog smsLog = this.smsLogService.findOneByBizId(bizId);

            SmsReport smsReport = JSON.parseObject(JSON.toJSONString(map), SmsReport.class);

            BeanUtils.copyProperties(smsReport,map);
            smsReport.setLog_id(smsLog.getId());

            this.smsReportRepository.save(smsReport);

            if(smsReport.getSuccess()) {
                //发送成功 商户短信数量 -1
                SmsHost smsHost = smsLog.getSmsAuthorize().getSmsHost();
                smsHost.setNumber(smsHost.getNumber() + 1);
                smsHost.setLeftNumber(smsHost.getLeftNumber() - 1);
                this.smsHostService.upsert(smsHost);
            } else {
                smsLog.setStatus(Boolean.FALSE);
                this.smsLogService.upsert(smsLog);
            }

        }
    }
}
