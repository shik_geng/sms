/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.controller;

import com.shikSMS.constant.ModelApplyConstants;
import com.shikSMS.constant.SysConstants;
import com.shikSMS.domain.SmsModel;
import com.shikSMS.manage.service.SmsModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author gengshikun
 * @date 2017/11/24
 */
@Controller
@RequestMapping(value = "model")
public class ModelController {

    private static final Logger logger = LoggerFactory.getLogger(ModelController.class);

    @Autowired
    private SmsModelService smsModelService;

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(String query, Integer pageNo, Model model) {
        Page pager = this.smsModelService.getListForPage(query, pageNo);
        model.addAttribute("pager", pager);
        model.addAttribute("pageNo", pageNo == null ? 1 : pageNo);
        return "model/list";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add() {
        return "model/add";
    }

    @RequestMapping(value = "upsert", method = RequestMethod.POST)
    @ResponseBody
    public Boolean save(SmsModel smsModel) {
        this.smsModelService.upsert(smsModel);
        return Boolean.TRUE;
    }

    @RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable String id, Model model) {
        SmsModel smsModel = this.smsModelService.findOne(id);
        model.addAttribute("smsModel", smsModel);
        return "model/edit";
    }

}
