<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>管理员添加</title>
    <#include "../comm/upms-base.ftl" />
    <#include "../comm/upms-layui.ftl" />
    <link rel="stylesheet" href="../../../static/css/global.css"/>
</head>
<body class="shik_body">
    <div class="layui-row">
        <div class="layui-col-xs12">
            <form id="shik_model_form" class="layui-form layui-form-pane" method="post">
                <input type="hidden" name="id" value="${smsModelApply.id}">
                <div class="layui-form-item">
                    <label class="layui-form-label">模板名称</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" name="title" lay-verify="required"
                               value="${smsModelApply.title}" autocomplete="off" placeholder="请输入模板名称" />
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">选择签名</label>
                    <div class="layui-input-block">
                        <select name="signId" lay-verify="required" lay-filter="authorize_type" lay-search="">
                            <option value=""></option>
                            <#list signs as sign>
                                <#if smsModelApply.signId?? && sign.id == smsModelApply.signId>
                                    <option selected value="${sign.id}">${sign.title}</option>
                                <#else >
                                    <option value="${sign.id}">${sign.title}</option>
                                </#if>
                            </#list>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item model_B00">
                    <label class="layui-form-label">模板CODE</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" name="smsCode" lay-verify="required"
                               autocomplete="off" placeholder="请输入模板CODE" />
                    </div>
                </div>
                <div class="layui-form-item layui-form-text model_B01">
                    <label class="layui-form-label">短信模板</label>
                    <div class="layui-input-block">
                        <textarea disabled placeholder="请输入模板内容" lay-verify="required" class="layui-textarea">${smsModelApply.modelContent}</textarea>
                    </div>
                </div>
                <div class="layui-form-item layui-form-text model_B01">
                    <label class="layui-form-label">申请描述</label>
                    <div class="layui-input-block">
                        <textarea disabled placeholder="请输入模板内容" lay-verify="required" class="layui-textarea">${smsModelApply.applyDesc}</textarea>
                    </div>
                </div>
                <div class="layui-layer-btn layui-layer-btn-c">
                    <a class="layui-layer-btn0 model_pass" lay-submit lay-filter="model_pass">通过</a>
                    <a class="layui-layer-btn1 model_refuse" lay-filter="model_refuse">拒绝</a>
                </div>
            </form>
        </div>
    </div>
</body>
<script type="text/javascript" src="../../../static/js/model.js"></script>
<script>
</script>
</html>
