/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.controller;

import com.alibaba.fastjson.JSON;
import com.shikSMS.config.DataSourceConfig;
import com.shikSMS.domain.SmsAuthorize;
import com.shikSMS.domain.SmsHost;
import com.shikSMS.manage.service.SmsAuthorizeService;
import com.shikSMS.manage.service.SmsSendService;
import com.shikSMS.support.util.RequestUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author gengshikun
 * @date 2017/11/25
 */
@Controller
@RequestMapping
public class SendController {

    private static final Logger logger = LoggerFactory.getLogger(SendController.class);

    @Autowired
    private SmsAuthorizeService smsAuthorizeService;

    @Autowired
    private SmsSendService smsSendService;


    /**
     * @param sendKey authorizeId
     * @return
     */
    @RequestMapping(value = "send")
    @ResponseBody
    public String index(String sendKey, String publicKey, HttpServletRequest request) {

        Map<String, String> params = RequestUtil.params2Map(request);
        logger.info(">>>>>>> params: {} >>>>>>>>>>>", JSON.toJSONString(params));

        // 判空

        // 验证参数key

        // 发送短信

        // 返回结果

        // 根据结果, 是否入队列通知


        SmsAuthorize smsAuthorize = this.smsAuthorizeService.findOne(sendKey);

        SmsHost smsHost = smsAuthorize.getSmsHost();
        logger.info(">>>>>>> 比较publickay: {} >>>>>>>>>>>", smsHost.getPublicKey());
        logger.info(">>>>>>> 比较publickay: {} >>>>>>>>>>>", publicKey);
        if (StringUtils.equals(smsHost.getPublicKey(), publicKey)) {
            try {

                this.smsSendService.sendSms(smsAuthorize, params.get("phones"), params.get("params"));

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            logger.info(">>>>>>> 秘钥不正确: {} >>>>>>>>>>>", publicKey);
            return "秘钥不正确";
        }

        return "success";
    }

}
