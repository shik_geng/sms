<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>管理员添加</title>
    <#include "../comm/upms-base.ftl" />
    <#include "../comm/upms-layui.ftl" />
    <link rel="stylesheet" href="../../../static/css/global.css"/>
</head>
<body class="shik_body">
    <div class="layui-row">
        <div class="layui-col-xs12">
            <form id="shik_authorize_form" class="layui-form layui-form-pane" action="/authorize/update" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">模板名称</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" name="title" lay-verify="required"
                               autocomplete="off" placeholder="请输入模板名称" />
                    </div>
                </div>
                <div class="layui-form-item" pane>
                    <label class="layui-form-label">是否启用</label>
                    <div class="layui-input-block">
                        <input type="checkbox" checked="" name="status" lay-skin="switch" lay-filter="switchTest" title="开关">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">选择商户</label>
                    <div class="layui-input-block">
                        <select name="hostId" lay-verify="required" lay-filter="authorize_type" lay-search="">
                            <option value=""></option>
                            <#list hosts as host>
                                <option value="${host.id}">${host.title}</option>
                            </#list>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">选择签名</label>
                    <div class="layui-input-block">
                        <select name="signId" lay-verify="required" lay-filter="authorize_type" lay-search="">
                            <option value=""></option>
                            <#list signs as sign>
                                <option value="${sign.id}">${sign.title}</option>
                            </#list>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">选择模板</label>
                    <div class="layui-input-block">
                        <select name="modelId" lay-verify="required" lay-filter="authorize_type" lay-search="">
                            <option value=""></option>
                            <#list models as model>
                                <option value="${model.id}">${model.title}</option>
                            </#list>
                        </select>
                    </div>
                </div>
                <div class="layui-layer-btn layui-layer-btn-c">
                    <a class="layui-layer-btn0" lay-submit lay-filter="authorize_submit">确认</a>
                    <a class="layui-layer-btn1 authorize_cancel" lay-filter="authorize_cancel">取消</a>
                </div>
            </form>
        </div>
    </div>
</body>
<script type="text/javascript" src="../../../static/js/authorize.js"></script>
<script>
</script>
</html>
