/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.service.impl;

import com.shikSMS.domain.SmsAuthorize;
import com.shikSMS.manage.service.SmsAuthorizeService;
import com.shikSMS.repository.SmsAuthorizeRepository;
import com.shikSMS.support.util.BeanUtils;
import com.shikSMS.support.util.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author gengshikun
 * @date 2017/11/25
 */
@Service
@Transactional
public class SmsAuthorizeServiceImpl implements SmsAuthorizeService {

    @Autowired
    private SmsAuthorizeRepository smsAuthorizeRepository;

    @Override
    public SmsAuthorize upsert(SmsAuthorize smsAuthorize) {


        if(StringUtils.isBlank(smsAuthorize.getId())) {
            smsAuthorize.setCreateTime(System.currentTimeMillis());
            smsAuthorize.setDeleteBoolean(Boolean.FALSE);
        } else {
            SmsAuthorize oldSmsAuthorize = this.smsAuthorizeRepository.findOne(smsAuthorize.getId());
            smsAuthorize.setUpdateTime(System.currentTimeMillis());
            BeanUtils.copyProperties(smsAuthorize, oldSmsAuthorize);
            smsAuthorize = oldSmsAuthorize;
        }

        return this.smsAuthorizeRepository.saveAndFlush(smsAuthorize);
    }

    @Override
    public Page getListForPage(String query, Integer pageNo) {
        Page<SmsAuthorize> pager = null;

        if (StringUtils.isNotBlank(query)) {
            pager = this.smsAuthorizeRepository.findByTitleLikeAndDeleteBoolean(query, Boolean.FALSE, PageUtils.buildPageRequest(pageNo));
        } else {
            pager = this.smsAuthorizeRepository.findAllByDeleteBoolean(Boolean.FALSE, PageUtils.buildPageRequest(pageNo));
        }

        return pager;
    }

    @Override
    public SmsAuthorize findOne(String id) {
        return this.smsAuthorizeRepository.findOne(id);
    }


}
