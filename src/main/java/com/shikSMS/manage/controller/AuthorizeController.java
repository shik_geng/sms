/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.controller;

import com.shikSMS.domain.SmsAuthorize;
import com.shikSMS.domain.SmsHost;
import com.shikSMS.domain.SmsModel;
import com.shikSMS.domain.SmsSign;
import com.shikSMS.manage.service.SmsAuthorizeService;
import com.shikSMS.manage.service.SmsHostService;
import com.shikSMS.manage.service.SmsModelService;
import com.shikSMS.manage.service.SmsSignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author gengshikun
 * @date 2017/11/25
 */
@Controller
@RequestMapping(value = "authorize")
public class AuthorizeController {

    private static final Logger logger = LoggerFactory.getLogger(ModelController.class);

    @Autowired
    private SmsAuthorizeService smsAuthorizeService;

    @Autowired
    private SmsHostService smsHostService;

    @Autowired
    private SmsSignService smsSignService;

    @Autowired
    private SmsModelService smsModelService;

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(String query, Integer pageNo, Model model) {
        Page pager = this.smsAuthorizeService.getListForPage(query, pageNo);
        model.addAttribute("pager", pager);
        model.addAttribute("pageNo", pageNo == null ? 1 : pageNo);
        return "authorize/list";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(Model model) {

        List<SmsHost> hosts = this.smsHostService.findAll();
        List<SmsSign> signs = this.smsSignService.findAll();
        List<SmsModel> models = this.smsModelService.findAll();
        model.addAttribute("hosts", hosts);
        model.addAttribute("signs", signs);
        model.addAttribute("models", models);

        return "authorize/add";
    }

    @RequestMapping(value = "upsert", method = RequestMethod.POST)
    @ResponseBody
    public Boolean save(SmsAuthorize smsAuthorize) {
        this.smsAuthorizeService.upsert(smsAuthorize);
        return Boolean.TRUE;
    }

    @RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable String id, Model model) {
        SmsAuthorize smsAuthorize = this.smsAuthorizeService.findOne(id);
        model.addAttribute("smsAuthorize", smsAuthorize);

        List<SmsHost> hosts = this.smsHostService.findAll();
        List<SmsSign> signs = this.smsSignService.findAll();
        List<SmsModel> models = this.smsModelService.findAll();
        model.addAttribute("hosts", hosts);
        model.addAttribute("signs", signs);
        model.addAttribute("models", models);
        return "authorize/edit";
    }

}
