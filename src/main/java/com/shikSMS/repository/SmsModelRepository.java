/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.repository;

import com.shikSMS.domain.SmsModel;
import com.shikSMS.domain.SmsSign;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;


/**
 * @author gengshikun
 * @date 2017/11/24
 */
@CacheConfig(cacheNames = "model")
public interface SmsModelRepository extends PagingAndSortingRepository<SmsModel, String>, JpaRepository<SmsModel, String> {

    Page<SmsModel> findByDeleteBoolean(Boolean deleteBoolean, Pageable pageable);
    Page<SmsModel> findByTitleLikeAndDeleteBoolean(String query, Boolean deleteBoolean, Pageable pageable);

    List<SmsModel> findByStatusAndDeleteBoolean(Boolean status, Boolean deleteBoolean);
}
