<script>
    <#if pager??>
        var pageTotal = ${pager.totalPages}; // 总页数
            pageCount = ${pager.totalElements}; // 总数量
            pageNumber = ${pager.number};
            pageSize = ${pager.size}; // 每页数量
            pageNo = ${pageNo}; // 页码
    <#else>
        var pageTotal = 0; // 总页数
            pageCount = 0; // 总数量
            pageNumber = 0;
            pageSize = 0; // 每页数量
            pageNo = 0; // 页码
    </#if>

    layui.use(['laypage'], function () {
        var laypage = layui.laypage;

        //完整功能
        laypage.render({
            elem: 'shik_page'
            , count: pageCount
            , limit: pageSize
            , curr: pageNo
            , layout: ['count', 'prev', 'page', 'next', 'skip']
            , jump: function (obj, first) {
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit); //得到每页显示的条数

                //首次不执行
                if (!first) {
                    active['query'].call(this, obj.curr);
                }
            }
        });
    })
</script>
