/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.controller;

import com.shikSMS.domain.SmsLog;
import com.shikSMS.manage.service.SmsLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author gengshikun
 * @date 2017/11/28
 */
@Controller
@RequestMapping(value = "log")
public class LogController {

    @Autowired
    private SmsLogService smsLogService;

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(String query, Integer pageNo, Model model) {
        Page pager = this.smsLogService.getListForPage(query, pageNo);
        model.addAttribute("pager", pager);
        model.addAttribute("pageNo", pageNo == null ? 1 : pageNo);
        return "log/list";
    }

    @RequestMapping(value = "{id}/view", method = RequestMethod.GET)
    public String view(@PathVariable String id, Model model) {
        SmsLog smsLog = this.smsLogService.findOne(id);
        model.addAttribute("smsLog", smsLog);
        return "log/view";
    }

}
