/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * @author gengshikun
 * @date 2017/11/22
 */
@Data
@Entity(name = "sms_authorize")
public class SmsAuthorize {

    @Id
    @GenericGenerator(name = "idGenerator", strategy = "uuid")
    @GeneratedValue(generator = "idGenerator")
    @Column(length = 32)
    private String id;

    @Column(nullable = false, length = 32)
    private String title;

    @Column
    private Boolean status = Boolean.FALSE;

    @Column(updatable = false)
    private Long createTime;

    @Column
    private Long updateTime;

    @Column
    private Boolean deleteBoolean;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="modelId",referencedColumnName="id",insertable=false,updatable=false)
    private SmsModel smsModel;
    private String modelId;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="signId",referencedColumnName="id",insertable=false,updatable=false)
    protected SmsSign smsSign;
    private String signId;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="hostId",referencedColumnName="id",insertable=false,updatable=false)
    private SmsHost smsHost;
    private String hostId;

    public SmsAuthorize(){}

    public SmsAuthorize(String title, Boolean status, String modelId, String signId, String hostId) {
        this.title = title;
        this.status = status;
        this.modelId = modelId;
        this.signId = signId;
        this.hostId = hostId;
    }
}
