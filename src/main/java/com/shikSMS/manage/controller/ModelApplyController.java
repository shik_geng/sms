/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.controller;

import com.shikSMS.domain.SmsModelApply;
import com.shikSMS.domain.SmsSign;
import com.shikSMS.manage.service.SmsModelApplyService;
import com.shikSMS.manage.service.SmsSignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author gengshikun
 * @date 2017/12/26
 */
@Controller
@RequestMapping(value = "model_apply")
public class ModelApplyController {

    @Autowired
    private SmsModelApplyService smsModelApplyService;

    @Autowired
    private SmsSignService smsSignService;

    /**
     * 申请模板
     * params.    title, modelContent, applyDesc, adminId, signId, publicKey, templateId
     *
     * @param smsModelApply
     * @return
     */
    @RequestMapping(value = "apply", method = RequestMethod.POST)
    @ResponseBody
    public Boolean apply(SmsModelApply smsModelApply, String publicKey) {
        this.smsModelApplyService.apply(smsModelApply, publicKey);
        return Boolean.TRUE;
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(String query, Integer pageNo, Model model) {
        Page pager = this.smsModelApplyService.getListForPage(query, pageNo);
        model.addAttribute("pager", pager);
        model.addAttribute("pageNo", pageNo == null ? 1 : pageNo);
        return "modelApply/list";
    }

    @RequestMapping(value = "{id}/varify", method = RequestMethod.GET)
    public String varify(@PathVariable String id, Model model) {
        SmsModelApply smsModelApply = this.smsModelApplyService.findOne(id);
        List<SmsSign> signs = this.smsSignService.findAll();
        model.addAttribute("smsModelApply", smsModelApply);
        model.addAttribute("signs", signs);
        return "modelApply/varify";
    }

    @RequestMapping(value = "{id}/pass", method = RequestMethod.POST)
    @ResponseBody
    public Boolean pass(@PathVariable String id, String signId, String smsCode) {
        this.smsModelApplyService.pass(id, signId, smsCode);
        return Boolean.TRUE;
    }

    @RequestMapping(value = "{id}/refuse", method = RequestMethod.POST)
    @ResponseBody
    public Boolean refuse(@PathVariable String id, String refuseReason) {
        this.smsModelApplyService.refuse(id, refuseReason);
        return Boolean.TRUE;
    }

    @RequestMapping(value = "{id}/info", method = RequestMethod.GET)
    public String info(@PathVariable String id, Model model) {
        SmsModelApply smsModelApply = this.smsModelApplyService.findOne(id);
        model.addAttribute("smsModelApply", smsModelApply);
        return "modelApply/info";
    }

}
