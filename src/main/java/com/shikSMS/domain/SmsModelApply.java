/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author gengshikun
 * @date 2017/12/26
 */
@Data
@Entity(name = "sms_model_apply")
public class SmsModelApply implements Serializable {

    @Id
    @GenericGenerator(name = "idGenerator", strategy = "uuid") //这个是hibernate的注解/生成32位UUID
    @GeneratedValue(generator = "idGenerator")
    @Column(length = 32)
    private String id;

    @Column(nullable = false, length = 32)
    private String title;

    @Column
    private String modelContent;

    @Column(updatable = false)
    private Long createTime;

    @Column
    private Long updateTime;

    @Column
    private Boolean deleteBoolean;

    @Column(length = 1)
    private String varifyStatus; // ModelConstants 1. 待审核 2. 成功 3. 拒绝

    @Column
    private String applyDesc;

    @Column
    private String refuseReason;

    @Column
    private Long varifyTime;

    @Column
    private String signId; // 冗余签名, 审核用

    @Column
    private String hostId; // 冗余商户, 审核用

    @Column
    private String adminId; // 冗余主办方, 审核用

    @Column
    private String templateId; // 回调用

    @Column
    private String returnUrl; // 回调用

    @Column(name = "number")
    private Integer number;

}
