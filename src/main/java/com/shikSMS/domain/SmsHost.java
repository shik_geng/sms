/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * 短信商户
 * @author gengshikun
 * @date 2017/11/23
 */
@Data
@Entity(name = "sms_host")
public class SmsHost {

    @Id
    @GenericGenerator(name = "idGenerator", strategy = "uuid") //这个是hibernate的注解/生成32位UUID
    @GeneratedValue(generator = "idGenerator")
    @Column(length = 32)
    private String id;

    @Column(name = "title", length = 32)
    private String title;

    @Column(name = "number")
    private Integer number;

    @Column
    private Integer leftNumber;

    @Column(name = "status")
    private Boolean status = Boolean.FALSE;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private String publicKey;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private String privateKey;

    @Column
    private Long createTime;

    @Column
    private Long updateTime;

    @Column
    private Boolean deleteBoolean;

}
