/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.service.impl;

import com.shikSMS.domain.SmsHost;
import com.shikSMS.domain.SmsSign;
import com.shikSMS.manage.service.SmsSignService;
import com.shikSMS.repository.SmsSignRepository;
import com.shikSMS.support.util.BeanUtils;
import com.shikSMS.support.util.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author gengshikun
 * @date 2017/11/24
 */
@Service
@Transactional
public class SmsSignServiceImpl implements SmsSignService {

    @Autowired
    private SmsSignRepository smsSignRepository;

    @Override
    public void upsert(SmsSign smsSign) {

        if(StringUtils.isBlank(smsSign.getId())) {
            smsSign.setCreateTime(System.currentTimeMillis());
            smsSign.setDeleteBoolean(Boolean.FALSE);
        } else {
            SmsSign oldsmsSign = this.smsSignRepository.findOne(smsSign.getId());
            smsSign.setUpdateTime(System.currentTimeMillis());
            BeanUtils.copyProperties(smsSign, oldsmsSign);
            smsSign = oldsmsSign;
        }

        this.smsSignRepository.saveAndFlush(smsSign);
    }

    @Override
    public Page getListForPage(String query, Integer pageNo) {
        Page<SmsSign> pager = null;

        if (StringUtils.isNotBlank(query)) {
            pager = this.smsSignRepository.findByTitleLikeAndDeleteBoolean(query, Boolean.FALSE, PageUtils.buildPageRequest(pageNo));
        } else {
            pager = this.smsSignRepository.findByDeleteBoolean(Boolean.FALSE, PageUtils.buildPageRequest(pageNo));
        }

        return pager;
    }

    @Override
    public List<SmsSign> findAll() {
        return this.smsSignRepository.findByStatusAndDeleteBoolean(Boolean.TRUE, Boolean.FALSE);
    }

    @Override
    public SmsSign findOne(String id) {
        return this.smsSignRepository.findOne(id);
    }

    /*@Override
    public List<SmsSign> getListByAdminId(String adminId) {
        return this.smsSignRepository.findByStatusAndDeleteBooleanAndAdminId(Boolean.TRUE, Boolean.FALSE, adminId);
    }*/
}
