<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>管理员添加</title>
    <#include "../comm/upms-base.ftl" />
    <#include "../comm/upms-layui.ftl" />
    <link rel="stylesheet" href="../../../static/css/global.css"/>
    <link rel="stylesheet" href="../../../static/css/host.css"/>
</head>
<body class="shik_body">
    <div class="layui-row">
        <div class="layui-col-xs12">
            <form id="shik_host_form" class="layui-form layui-form-pane" action="/host/upsert" method="post">
                <input type="hidden" name="id" value="${smsHost.id}">
                <div class="layui-form-item">
                    <label class="layui-form-label">商户名称</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" name="title" lay-verify="required"
                               value="${smsHost.title}" autocomplete="off" placeholder="请输入商户名称" />
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">商户名称</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" name="leftNumber" lay-verify="number"
                               value="${smsHost.leftNumber}" autocomplete="off" placeholder="请输入商户名称" />
                    </div>
                </div>
                <div class="layui-form-item" pane>
                    <label class="layui-form-label">是否启用</label>
                    <div class="layui-input-block">
                        <input type="checkbox" ${smsHost.status?then('checked=""', '')} name="status" lay-skin="switch" lay-filter="switchTest" title="开关">
                    </div>
                </div>
                <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">publicKey</label>
                    <div class="layui-input-block">
                        <textarea disabled placeholder="请输入模板内容" lay-verify="required" class="layui-textarea shik_publicKey">${smsHost.publicKey}</textarea>
                    </div>
                </div>
                <div class="layui-layer-btn layui-layer-btn-c">
                    <a class="layui-layer-btn0" lay-submit lay-filter="host_submit">确认</a>
                    <a class="layui-layer-btn1 host_cancel" lay-filter="host_cancel">取消</a>
                </div>
            </form>
        </div>
    </div>
</body>
<script type="text/javascript" src="../../../static/js/host.js"></script>
<script>
</script>
</html>
