/**
 *
 * @author gengshikun
 * @date 2016/12/12
 */
layui.use(['form', 'layer', 'jquery'], function () {
    console.log(parent.layer === undefined)
    var form = layui.form;
        layer = layui.layer;
        $ = layui.jquery;
        laypage = layui.laypage;

    // sign_submit
    form.on('submit(sign_submit)', function(data){
        $.post('/sign/upsert', data.field, function(){
            closeIframe();
            parent.location.reload(); // 刷新
        }, 'json');
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });

    $('.sign_add').on('click', function() {
        layer.open({
            type: 2,
            title: '管理员添加',
            content: ['/sign/add', 'no'],
            area: ['440px', '320px']
        });
    });

    $('.sign_edit').on('click', function() {
        var id = $(this).attr('data_id');
        layer.open({
            type: 2,
            title: '管理员添加',
            content: ['/sign/'+id+'/edit', 'no'],
            area: ['440px', '320px']
        });
    });

    $('.sign_cancel').on('click', function() {
        closeIframe();
    })

    function closeIframe() {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
});

//触发事件
var active = {
    query: function(pageNo) {
        console.log('query:'+pageNo);
        console.log(location);

        location.href = location.origin + location.pathname + '?pageNo=' + pageNo;
    }
};
