/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.controller;

import com.shikSMS.manage.service.SmsReportService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author gengshikun
 * @date 2017/11/24
 */
@Controller
@RequestMapping
public class IndexController {

    @Autowired
    private SmsReportService smsReportService;

    @RequestMapping(value = {"", "/index"}, method = RequestMethod.GET)
    public String index() {
        return "index";
    }


    @RequestMapping(value = "sms/report", method = RequestMethod.POST)
    @ResponseBody
    public String smsStatusReport(HttpServletRequest request) throws IOException {
        try {
            ServletInputStream is = request.getInputStream();
            String list = IOUtils.toString(is, "utf-8");
            System.out.println(list);

            this.smsReportService.dealReport(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "{\n" +
                "    \"code\": 0,\n" +
                "    \"msg\": \"成功\"\n" +
                "}";
    }

}
