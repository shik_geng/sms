/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.manage.service.impl;

import com.shikSMS.constant.ModelApplyConstants;
import com.shikSMS.domain.SmsModel;
import com.shikSMS.manage.service.SmsModelService;
import com.shikSMS.repository.SmsModelRepository;
import com.shikSMS.support.util.BeanUtils;
import com.shikSMS.support.util.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author gengshikun
 * @date 2017/11/24
 */
@Service
@Transactional
public class SmsModelServiceImpl implements SmsModelService {

    @Autowired
    private SmsModelRepository smsModelRepository;

    @Override
    public SmsModel upsert(SmsModel smsModel) {

        if(StringUtils.isBlank(smsModel.getId())) {
            smsModel.setCreateTime(System.currentTimeMillis());
            smsModel.setDeleteBoolean(Boolean.FALSE);
        } else {
            SmsModel oldSmsSign = this.smsModelRepository.findOne(smsModel.getId());
            smsModel.setUpdateTime(System.currentTimeMillis());
            BeanUtils.copyProperties(smsModel, oldSmsSign);
            smsModel = oldSmsSign;
        }

        return this.smsModelRepository.saveAndFlush(smsModel);
    }

    @Override
    public Page getListForPage(String query, Integer pageNo) {
        Page<SmsModel> pager = null;

        if (StringUtils.isNotBlank(query)) {
            pager = this.smsModelRepository.findByTitleLikeAndDeleteBoolean(query, Boolean.FALSE, PageUtils.buildPageRequest(pageNo));
        } else {
            pager = this.smsModelRepository.findByDeleteBoolean(Boolean.FALSE, PageUtils.buildPageRequest(pageNo));
        }

        return pager;
    }

    @Override
    public List<SmsModel> findAll() {
        return this.smsModelRepository.findByStatusAndDeleteBoolean(Boolean.TRUE, Boolean.FALSE);
    }

    @Override
    public SmsModel findOne(String id) {
        return this.smsModelRepository.findOne(id);
    }

    /*@Override
    public void pass(String id, String smsCode) {
        SmsModel smsModel = this.findOne(id);
        smsModel.setVarifyStatus(ModelApplyConstants.SMS_MODEL_VARIFY_STATUS_OK);
        smsModel.setStatus(Boolean.TRUE);
        smsModel.setVarifyTime(System.currentTimeMillis());
        smsModel.setSmsCode(smsCode);
        this.upsert(smsModel);

        // TODO 社群通
    }

    @Override
    public void refuse(String id, String refuseReason) {
        SmsModel smsModel = this.findOne(id);
        smsModel.setVarifyStatus(ModelApplyConstants.SMS_MODEL_VARIFY_STATUS_REFUSE);
        smsModel.setVarifyTime(System.currentTimeMillis());
        smsModel.setRefuseReason(refuseReason);
        smsModel.setSmsCode("");
        this.upsert(smsModel);

        // TODO 社群通
    }*/
}
