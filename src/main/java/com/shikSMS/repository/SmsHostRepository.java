/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.repository;

import com.shikSMS.domain.SmsHost;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;


/**
 * @author gengshikun
 * @date 2017/11/24
 */
@CacheConfig(cacheNames = "host")
public interface SmsHostRepository extends PagingAndSortingRepository<SmsHost, String>, JpaRepository<SmsHost, String> {

    Page<SmsHost> findByDeleteBoolean(Boolean deleteBoolean, Pageable pageable);
    Page<SmsHost> findByTitleLikeAndDeleteBoolean(String query, Boolean deleteBoolean, Pageable pageable);

    List<SmsHost> findByStatusAndDeleteBoolean(Boolean status, Boolean deleteBoolean);

    SmsHost findByPublicKey(String publicKey);
}
