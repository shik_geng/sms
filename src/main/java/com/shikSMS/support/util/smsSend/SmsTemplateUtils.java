package com.shikSMS.support.util.smsSend;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author gengshikun
 * @date 2017/12/22
 */
public class SmsTemplateUtils {

    public static final String PASS = "pass";
    public static final String REFUSE = "refuse";

    /**
     * 调用短信服务 -- 通过
     *
     * @param sendUrl 短信回调地址
     * @param id      id
     * @param sendKey sendKey
     * @throws Exception
     */
    public static void smsPass(String sendUrl, String id, String sendKey) throws Exception {
        //发短信
        Map httpParams = Maps.newHashMap();
        httpParams.put("id", id);
        httpParams.put("sendKey", sendKey);
        HttpUtils.postParameters(sendUrl + PASS, httpParams);
    }

    /**
     * 调用短信服务 -- 通过
     *
     * @param sendUrl 短信回调地址
     * @param id      id
     * @param refuseReason refuseReason
     * @throws Exception
     */
    public static void smsRefuse(String sendUrl, String id, String refuseReason) throws Exception {
        //发短信
        Map httpParams = Maps.newHashMap();
        httpParams.put("id", id);
        httpParams.put("refuseReason", refuseReason);
        HttpUtils.postParameters(sendUrl + REFUSE, httpParams);
    }

}
