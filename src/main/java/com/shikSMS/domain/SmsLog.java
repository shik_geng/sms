/**
 * ━━━━━━神兽出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃神兽保佑, 永无BUG!
 * 　　　　┃　　　┃Code is far away from bug with the animal protecting
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━感觉萌萌哒━━━━━━
 */
package com.shikSMS.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

/**
 * @author gengshikun
 * @date 2017/11/17
 */
@Data
@Entity(name = "sms_log")
public class SmsLog {

    @Id
    @GenericGenerator(name = "idGenerator", strategy = "uuid") //这个是hibernate的注解/生成32位UUID
    @GeneratedValue(generator = "idGenerator")
    @Column(length = 32)
    private String id;

    @OneToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="authorizeId",referencedColumnName="id",insertable=false,updatable=false)
    private SmsAuthorize smsAuthorize;
    private String authorizeId;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private String phone;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private String params;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private String smsContent;

    @Column(name = "number")
    private Integer number;

    @Column
    private Boolean status;

    @Column(updatable = false)
    private Long createTime;

    @Column
    private Long noticeTime;

    @Column
    private String bizId;

    @Column
    private String errCode;

    @Column
    private String errMsg;

    @OneToMany(fetch=FetchType.EAGER)
    @JoinColumn(name="log_id")
    private List<SmsReport> smsReportList;

    public SmsLog(){}

    public SmsLog(String authorizeId, String phone, String params, String smsContent, Integer number, String bizId, String errCode, String errMsg) {
        this.authorizeId = authorizeId;
        this.phone = phone;
        this.params = params;
        this.smsContent = smsContent;
        this.number = number;
        this.bizId = bizId;
        this.errCode = errCode;
        this.errMsg = errMsg;

        this.status = Boolean.FALSE;
        this.createTime = System.currentTimeMillis();
    }
}
