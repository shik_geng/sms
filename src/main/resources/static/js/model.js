/**
 *
 * @author gengshikun
 * @date 2016/12/12
 */
layui.use(['form', 'layer', 'jquery'], function () {
    console.log(parent.layer === undefined)
    var form = layui.form;
        layer = layui.layer;
        $ = layui.jquery;

    // model_type
    form.on('select(model_type)', function(data){
        // console.log(data.elem); //得到select原始DOM对象
        // console.log(data.value); //得到被选中的值
        // console.log(data.othis); //得到美化后的DOM对象
        switch (data.value) {
            case 'B00': // 阿里
                $('.model_B00').find('input').attr('lay-verify', 'required');
                break;
            case 'B01': // 253
                $('.model_B00').find('input').attr('lay-verify', '');
                break;
        }
        form.render();
    });

    // model_submit
    form.on('submit(model_submit)', function(data){
        $.post('/model/upsert', data.field, function(){
            closeIframe();
            parent.location.reload(); // 刷新
        }, 'json');
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });

    $('.model_add').on('click', function() {
        layer.open({
            type: 2,
            title: '管理员添加',
            content: ['/model/add', 'no'],
            area: ['520px', '470px']
        });
    });

    $('.model_edit').on('click', function() {
        var id = $(this).attr('data_id');
        layer.open({
            type: 2,
            title: '管理员添加',
            content: ['/model/'+id+'/edit', 'no'],
            area: ['520px', '470px']
        });
    });

    $('.model_varify').on('click', function() {
        var id = $(this).attr('data_id');
        layer.open({
            type: 2,
            title: '模板审核',
            content: ['/model_apply/'+id+'/varify', 'no'],
            area: ['520px', '570px']
        });
    });

    // model_submit
    form.on('submit(model_pass)', function(data){
        var id = $('input[name="id"]').val();
        // var smsCode = $('input[name="smsCode"]').val();
        $.post('/model_apply/'+id+'/pass', data.field, function(){
            closeIframe();
            parent.location.reload(); // 刷新
        }, 'json');
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });

    $('.model_refuse').on('click', function() {
        var id = $('input[name="id"]').val();
        layer.open({
            type: 1,
            title: '确认拒绝该模板吗?',
            content: '<textarea placeholder="请填写拒绝原因" style="margin: 20px; height: 95px; width: 328px"></textarea>',
            area: ['370px', '240px'],
            btn: ['确认', '取消'],
            yes: function(index, layero){
                //按钮【按钮一】的回调
                var refuseReason = layero.find('textarea').val();
                if(refuseReason == '') {
                    layer.msg('请填写拒绝原因')
                } else {
                    $.post('/model_apply/'+id+'/refuse', {id:id, refuseReason:refuseReason}, function(){
                        closeIframe();
                        parent.location.reload(); // 刷新
                    }, 'json');
                }
            },
            btn2: function(index, layero){
                //按钮【按钮二】的回调

                //return false 开启该代码可禁止点击该按钮关闭
            }
        });
    });

    $('.model_info').on('click', function() {
        var id = $(this).attr('data_id');
        var varifyStatus = $(this).attr('data_varify');
        var height = '620px';
        if(varifyStatus == '2') {
            height = '460px'
        }
        layer.open({
            type: 2,
            title: '模板信息',
            content: ['/model_apply/'+id+'/info', 'no'],
            area: ['520px', height]
        });
    });

    $('.model_cancel').on('click', function() {
        closeIframe();
    });

    function closeIframe() {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
});

//触发事件
var active = {
    query: function(pageNo) {
        console.log('query:'+pageNo);
        console.log(location);

        location.href = location.origin + location.pathname + '?pageNo=' + pageNo;
    }
};
