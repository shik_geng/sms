<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>管理员列表</title>
<#include "../comm/upms-base.ftl" />
<#include "../comm/upms-layui.ftl" />
    <link rel="stylesheet" href="../../../static/css/global.css"/>
</head>
<body class="shik_body">
<div class="layui-row">

    <div class="layui-col-xs12">
        <form class="log_form">
            <div class="layui-col-md3">
                <label class="layui-form-label">手机号</label>
                <div class="layui-input-inline">
                    <input type="tel" name="query" lay-verify="required|phone" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-col-md5">
                <label class="layui-form-label">发送时间</label>
                <div class="layui-input-inline">
                    <input type="text" name="date1" id="date1" autocomplete="off" class="layui-input">
                </div>
                <span> - </span>
                <div class="layui-input-inline">
                    <input type="text" name="date2" id="date2" autocomplete="off" class="layui-input">
                </div>
            </div>
        </form>
        <div class="layui-col-md1">
            <button class="layui-btn layui-btn-primary shik_btn" data-method="query"><i class="layui-icon">&#xe615;</i></button>
        </div>
    </div>

    <div class="layui-col-xs12">
        <table class="layui-table" lay-skin="line">
            <colgroup>
                <col width="150">
                <col width="150">
                <col width="150">
                <col width="150">
                <col width="200">
                <col>
            </colgroup>
            <thead>
            <tr>
                <th>商户名称</th>
                <th>发送手机</th>
                <th>模板名称</th>
                <th>发送状态</th>
                <th>发送信息</th>
                <th>发送参数</th>
                <th>发送内容</th>
                <th>创建时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
                <#list pager.content as log>
                    <tr data_id="${log.id}">
                        <td>${log.smsAuthorize.smsHost.title}</td>
                        <td>${log.phone}</td>
                        <td>${log.smsAuthorize.smsModel.title}</td>
                        <td>${log.status?then('成功', '失败')}</td>
                        <td>${log.errMsg}</td>
                        <td>${log.params}</td>
                        <td>【${log.smsAuthorize.smsSign.title}】${log.smsContent}</td>
                        <td>${log.createTime?number_to_datetime}</td>
                        <td><a class="log_view">查看</a></td>
                    </tr>
                </#list>
            </tbody>
        </table>
    </div>

    <div class="layui-col-xs12">
        <div class="shik_footer" id="shik_page"></div>
    </div>
</div>
</body>
<#include "../comm/sms-page.ftl" />
<script type="text/javascript" src="../../../static/js/log.js"></script>
</html>
